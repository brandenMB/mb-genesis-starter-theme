<div class="cell services__item">
    <a class='services__link trigger' href="<?php echo $service_icon ?>">
        <div class="services__icon">
            <?php echo file_get_contents( $service_icon ); ?>
        </div>
		<h4 class="home-services__title"><?php echo $service_text ?></h4>
	</a>
</div>