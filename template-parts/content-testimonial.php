<div class="testimonial__item" itemprop="review" itemscope itemtype="http://schema.org/Review">
    <blockquote>
        <?php the_content();?>
        <footer>
            <span class="testimonials__name" itemprop="name">-
                <?php the_title();?>
            </span>
            <div itemprop="reviewRating" itemscope itemtype="http://schema.org/Rating">
                <span class="screen-reader-text" itemprop="ratingValue">5 stars</span>
                <img class="testimonials__stars" src="<?php echo get_theme_file_uri('assets/images/slick/five-stars.svg'); ?>" alt="Five Star Criminal Defense Lawyer San Marcos"
                />
            </div>
        </footer>
    </blockquote>
</div>