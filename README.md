# Mockingbird Genesis Starter Theme

Child theme used for Genesis projects

## Getting Started

These instructions will get you a copy of the project up and running on your local machine for development and testing purposes. 

### Prerequisites

Have Genesis Theme Installed
ACF Plugin Installed
Import ACF Fields

### Installing

A step by step series of examples that tell you have to get the theme installed

Download Theme
Install Theme via WordPress or unzip and add to **wp-content/themes**
Activate theme


## Theme Development

Install Dependencies from theme root.

```npm install```

Update **gulpfile.js** serve task proxy setting with dev domain.

```proxy: "domain.com"```

Run Dev Server and automatic tasks
Will watch for JS, SCSS files and compile.
Will live reload on localhost:8080

```gulp serve```

## Authors

* **Branden Youngs** - *Initial work* - [Mockingbird](https://mockingbirdmarketing.com)


## License

This project is licensed under the MIT License - see the [LICENSE.md](LICENSE.md) file for details

