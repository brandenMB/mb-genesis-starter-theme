<?php
/*
  Template Name: Home Page
 */

//* Add home body class
add_filter('body_class', 'mbird_home_body_class');
function mbird_home_body_class($classes) {

	$classes[] = 'homepage';
	return $classes;
}

//* Force full-width-content layout setting
add_filter('genesis_pre_get_option_site_layout', '__genesis_return_full_width_content');

//* Remove the entry header markup, entry title, and post info
//* Will place later
remove_action( 'genesis_entry_header', 'genesis_entry_header_markup_open', 5 );
remove_action( 'genesis_entry_header', 'genesis_entry_header_markup_close', 15 );
remove_action( 'genesis_entry_header', 'genesis_do_post_title' );
remove_action( 'genesis_entry_header', 'genesis_post_info', 12 );
remove_action( 'genesis_entry_content', 'genesis_do_post_content' );

//* Add Custom Content Areas
add_action( 'genesis_entry_content', 'mbird_home_content_insert', 1 );
function mbird_home_content_insert() { ?>
	<div class="hero home-hero">
		<div class="hero__cover bg-cover"></div>
		<div class="hero__image">
			<?php $hero_image = get_field('hero_background_image');
				echo wp_get_attachment_image( $hero_image, 'full' );
			?>
		</div>
		<div class="hero__copy">
			<?php the_field('hero_content');?>
		</div>
	</div><!--end hero -->
	<div class="contact home-contact blue-bg row-pad">
		<div class="wrap">
			<h2 class="centered"><?php the_field('contact_us_title');?></h2>
			<?php $contact_form_shortcode = get_field('contact_us_form');?>
			<?php echo do_shortcode($contact_form_shortcode); ?>
			</p>
		</div>
	</div><!--end home contact -->
	<div class="home-intro intro row-pad centered">
		<div class="wrap">
			<?php the_field('home_intro_content'); ?>
		</div>
	</div><!--end intro -->
	<?php mbird_add_footer_badges(); ?>
	<div class="meet-case row-pad subtle-bg">
		<div class="wrap">
			<div class="row">
				<div class="two-thirds first meet-case__copy">
					<h2><?php the_field('home_meet_case_title');?></h2>
					<?php the_field('home_meet_case_content');?>
				</div>
				<div class="one-third meet-case__picture">
					<?php 
						$meet_case_portrait = get_field('home_meet_case_portrait'); 
						echo wp_get_attachment_image( $meet_case_portrait, 'full', "", ["class" => "shadow"] );
					?>
				</div>
			</div>
		</div>
	</div>
	<div class="services home-services">
		<?php

			if( have_rows('home_services') ):
				echo '<div class="services__list flex-wrap f-break">';

				while ( have_rows('home_services') ) : the_row();
					$service_icon = get_sub_field('home_service_icon');
					$service_text = get_sub_field('home_service_text');
					$service_link = get_sub_field('home_service_link');

					include(locate_template('./template-parts/content-service.php'));

				endwhile;

				echo '</div>';
			else :

				// no rows found

			endif;

		?>
	</div>
	<div class="testimonials centered row-pad">
	<div class="wrap">
		<h2 class="t-caps">WHAT OUR CLIENTS ARE SAYING</h2>
	<div class="testimonials__container slider">
		<?php
			$testimonial_args = array( 'post_type' => 'testimonial', 'posts_per_page' => -1 );
			$testimonial_loop = new WP_Query( $testimonial_args );
			while ( $testimonial_loop->have_posts() ) : $testimonial_loop->the_post();
		?>
			<?php get_template_part( 'template-parts/content', 'testimonial' ); ?>
		<?php endwhile; wp_reset_postdata(); ?>
	</div> <!--end slider -->
</div>
	</div>
	<?php $page_content = get_the_content();

	if( $page_content ) {
		echo '<div class="row row-pad"><div class="wrap">'.$page_content.'</div></div>';
	}

}

genesis();
