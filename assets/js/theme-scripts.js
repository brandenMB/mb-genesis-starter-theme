// RESPONSIVE NAVIGATION
// ====================

jQuery(function($) {
    $("header .genesis-nav-menu, .nav-primary .genesis-nav-menu, .nav-secondary .genesis-nav-menu").addClass("responsive-menu").before('<div class="responsive-menu-icon"></div>');
    $(".responsive-menu-icon").click(function() {
        $(this).toggleClass("nav-open");
        $(this).next("header .genesis-nav-menu, .nav-primary .genesis-nav-menu, .nav-secondary .genesis-nav-menu").slideToggle();
    });
    $(window).resize(function() {
        if (window.innerWidth > 1024) {
            $("header .genesis-nav-menu, .nav-primary .genesis-nav-menu, .nav-secondary .genesis-nav-menu, nav .sub-menu").removeAttr("style");
            $(".responsive-menu > .menu-item, .responsive-menu > .menu-item > .sub-menu  > .menu-item").removeClass("menu-open");
        }
    });


    $(".responsive-menu > .menu-item").click(function(event) {
        if (event.target !== this) return;
        $(this).find(".sub-menu:first").slideToggle(function() {
            $(this).parent().toggleClass("menu-open");
        });
    });


    $(".sub-menu .menu-item").click(function(event) {
        $(this).find(".sub-menu:first").slideToggle(function() {
            $(this).parent().toggleClass("menu-open");
        });
    });
    $(document).ready(function() {

        //Hover Effect For Services
        $(".services__item .trigger").hover(function(e) {
            $(this).parent().toggleClass("hovered");
        });
        
        //Slick Slider For Testimonials
        var startSlider = document.querySelector(".testimonials");
        if(startSlider) {
            $('.slider').slick({
                adaptiveHeight: true,
                prevArrow:"<img class='slick-prev slick-arrow' src='" + casedarwinData.theme_url + "/assets/images/slick/left-arrow.svg'>",
                nextArrow:"<img class='slick-next slick-arrow' src='" + casedarwinData.theme_url + "/assets/images/slick/right-arrow.svg'>"
            });
        }
        var myElement = document.querySelector(".sticky-header");
        // construct an instance of Headroom, passing the element
        if(myElement) {
            var headroom = new Headroom(myElement, {
                "offset": 205,
                "tolerance": 5,
                "classes": {
                "initial": "animated",
                "pinned": "slideDown",
                "unpinned": "slideUp"
                }
            });
            // initialise
            headroom.init(); 
        }
                
    });

});
