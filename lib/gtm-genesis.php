<?php

/*
 Used to Create Google Tag Manager
 */

// Field defaults for text box - Google Tag Manager after opening body code
function mbird_gtm_defaults( $defaults ) {
    $defaults['gtm_body_code'] = '';
    $defaults['gtm_head_code'] = '';
	return $defaults;
}
add_filter( 'genesis_theme_settings_defaults', 'mbird_gtm_defaults' );

// Register GTM boxy settings box on genesis settings page
function mbird_register_gtm_settings_box( $_genesis_theme_settings_pagehook ) {
    add_meta_box( 'mbird-gtm-box', 'Google Tag Manager', 'mbird_gtm_box', $_genesis_theme_settings_pagehook, 'main' );
}
add_action( 'genesis_theme_settings_metaboxes', 'mbird_register_gtm_settings_box' );


// Register the GTM text area - opening body
function mbird_gtm_box() {
	?>
    <p>Immediately After Opening Head Tag Script: <br />
	<textarea class="large-text" rows="8" cols="78" name="<?php echo GENESIS_SETTINGS_FIELD; ?>[gtm_head_code]"><?php echo genesis_get_option('gtm_head_code'); ?></textarea></p>
    <p>Immediately After Opening Body Tag Script: <br />
	<textarea class="large-text" rows="8" cols="78" name="<?php echo GENESIS_SETTINGS_FIELD; ?>[gtm_body_code]"><?php echo genesis_get_option('gtm_body_code'); ?></textarea></p>

	<?php
}
// Insert Google tag field after opening body
add_action( 'genesis_before', 'mbird_insert_google_body_tag');
function mbird_insert_google_body_tag() {
    echo genesis_get_option('gtm_body_code');
}
// Insert Google tag field after head tag - have to redo the doctype function 
remove_action('genesis_doctype', 'genesis_do_doctype');
add_action( 'genesis_doctype', 'mbird_insert_google_head_tag');

function mbird_insert_google_head_tag() { ?>
    <!DOCTYPE html>
    <html <?php language_attributes( 'html' ); ?>>
    <head <?php echo genesis_attr( 'head' ); ?>>
        <?php echo genesis_get_option('gtm_head_code'); ?>
        <meta charset="<?php bloginfo( 'charset' ); ?>" />
<?php
}