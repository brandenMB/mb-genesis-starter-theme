<?php

//* Removing genesis header markup, content and nav for reordering
//
//* Remove header markup open
//remove_action( 'genesis_header', 'genesis_header_markup_open', 5 );

//* Remove header
//remove_action( 'genesis_header', 'genesis_do_header' );

//* Remove header markup close
//remove_action( 'genesis_header', 'genesis_header_markup_close', 15 );

//* Remove the Primary Nav
remove_action( 'genesis_after_header', 'genesis_do_nav' );

remove_action( 'genesis_after_header', 'genesis_do_subnav' );


// function mbird_add_language_button() { 
  
//   if (function_exists('icl_get_languages')) {
//       $languages = icl_get_languages('skip_missing=1');
//       if(count($languages) >= 2){
//         echo '<div class="lang-switcher">';
//           foreach($languages as $l){
//           if(!$l['active']) $langs[] = '<a class="button small-btn" href="'.$l['url'].'">'.$l['native_name'].'</a>';
//           }
//           echo join(' ', $langs);
//           echo '</div>';
//       }
//   }
// }

add_action('genesis_header_right', 'genesis_do_nav', 2);


add_action('genesis_header_right', 'mbird_add_header_info', 1);
/*
 * Add header contact info
 * 
 * @since 1.0.0
 * 
 * @return void
 */
function mbird_add_header_info() {
	$language_placeholder = get_theme_file_uri('assets/images/espanol-placeholder.png');
	if ( get_field('theme_phone', 'option') ) {
		echo '<div class="header-widget-area__top">';
		echo '<div class="lang-switcher">';
		echo '<img src="'.$language_placeholder.'"/>';
		echo '</div>';
		echo '<div class="header-info"><div class="header-phone">';          
			//echo '<span class="phone-cta">'.get_field("theme_phone_cta", "option").'</span>';
			echo '<span class="phone-num"><a href="tel:'.get_field("theme_phone_link", "option").'">'.get_field("theme_phone", "option").'</a></span>';
		echo '</div></div></div>';
	}

}


add_action( 'genesis_after_header', 'mb_mobile_nav' );
function mb_mobile_nav() {
  echo '<nav class="nav-primary mobile" itemscope="" itemtype="https://schema.org/SiteNavigationElement" id="genesis-nav-primary-mobile" aria-label="Main navigation"><div class="wrap">';
  wp_nav_menu( array( 'theme_location' => 'primary','menu_id' => 'mobile-menu', 'menu_class' => 'menu genesis-nav-menu menu-primary js-superfish responsive-menu') );
  echo '</nav> </div>';
}
